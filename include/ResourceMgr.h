#ifndef RESOURCE_MGR
#define RESOURCE_MGR

#include<vector>

using namespace std;

// TODO: more pirate names
using arrType = vector<vector<int>>;

class ResourceMgr
{

public:
    static arrType readGraph();

    static arrType generateRandomGraph(int size);
};

#endif