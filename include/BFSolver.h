#ifndef BF_SOLVER
#define BF_SOLVER

#include "./ResourceMgr.h"
#include "./OutputMgr.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include<bits/stdc++.h> 
#include "./solver.h"


class BFSolver: public Solver
{
    std::vector<int> permutations;


public:
    void solve(arrType map); 
    int get_current_permutation_cost(); 
    // void display_result();
};

#endif