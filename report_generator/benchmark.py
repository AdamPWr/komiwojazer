import subprocess
#import matplotlib.pyplot as plt
#import matplotlib
import sys

problem_sizes = [5,9,13,14,15,16,17] #[5, 10, 15, 20, 25, 30, 35] #[4,5,6,7,8,9,10] #
#problem_sizes = [30]
def get_results_for(method: str):

    avg_results = []

    for size in problem_sizes:

        results = []
        for i in range(25):
            ret_val = subprocess.check_output(f'../a.out {method} {size} | grep "#"', shell=True).decode()
            time = float(ret_val.replace("# ", ""))
            results.append(time)

        avg_result = sum(results)/len(results)

        print(f'{method:<2}, {size:<2}, {avg_result:8f}')
        avg_results.append(avg_result)

    return avg_results


# dp_res = get_results_for("dp")
# bf_res = get_results_for("bf")
# bb_res = get_results_for("bb")

get_results_for(sys.argv[1])


exit(0)

plt.plot(problem_sizes, bb_res, label="BB")
plt.plot(problem_sizes, bf_res, label="BF")
plt.plot(problem_sizes, dp_res, label="DP")

'''
plt.plot(problem_sizes, [0.0000477, 0.0003604, 0.00359, 0.0336, 0.4247, 5.5819, 90] , label='Brute force')
plt.plot(problem_sizes, [
0.00059,
0.00182,

0.00623,

0.02563,
0.09254,

0.31162,
1.34112,
] , label='Branch and bound')
'''

# plt.yscale('log')
plt.legend()

plt.show()
# plt.savefig("out.png")
# plt.show()
