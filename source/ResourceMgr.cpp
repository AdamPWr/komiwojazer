#include "../include/ResourceMgr.h"

#include <fstream>
#include <iostream>
#include <exception>
#include <random>
#include <chrono>

using namespace std;

arrType ResourceMgr::readGraph()
{

    unsigned int numberOfNodes;
    cout << "Podaj nazwe pliku" << endl; // nazwa powinna sie znajdowac w katalogu resource
    string name;
    cin >> name;
    name = "resources/" + name;

    ifstream plik;

    try
    {
        plik.open(name, ios::in);

        plik >> numberOfNodes;
        arrType arr(numberOfNodes, vector<int>(numberOfNodes));

        if (!plik.is_open())
            throw runtime_error("Nie otwarto pliku");

        int temp;
        auto row = 0; // TODO: czemu row jest definiowany 2 razy
        for (int row = 0; row < numberOfNodes; ++row)
        {
            for (int col = 0; col < numberOfNodes; ++col)
            {
                plik >> temp;
                arr[row][col] = temp;
            }
        }

        plik.close();
        return arr;
    }
    catch (const std::exception &e)
    {
        std::cout << e.what() << '\n';
        throw std::logic_error("Blad podczas odczytu z pliku o nazwie " + name);
    }
}

arrType ResourceMgr::generateRandomGraph(int size)
{
    arrType arr(size, vector<int>(size));

    size_t seed = std::chrono::steady_clock::now().time_since_epoch().count();
    std::default_random_engine engine(seed);
    std::uniform_int_distribution<int> distr(1, 9);

    for (int row = 0; row < size; ++row)
    {
        for (int col = 0; col < size; ++col)
        {
            if (row == col)
            {
                arr[row][col] = 0;
            }
            else
            {
                arr[row][col] = distr(engine);
            }
        }
    }

    return arr;
}