#include "../include/DPSolver.h"

using namespace std;

int DPSolver::count_ones_in_binary_representation(int value)
{

    bitset<30> x(value);

    int ones = 0;
    for (int i = 0; i < 30; i++)
    {

        if (x[i] == 1)
        {
            ones++;
        }
    }
    return ones;
}

int DPSolver::count_zeros_in_binary_representation(int value)
{
    return this->num_of_cities - count_ones_in_binary_representation(value);
}

// Must be called after DPSolver::solve
void DPSolver::restore_path()
{
    // cout << "restoring path..." << endl;

    vector<int> already_visited;

    for (int i = 1; i <= this->num_of_cities; i++)
    {
        int min_cost = INT_MAX;
        int vertice_with_min_cost = -1;

        // cout << i << " zeroes" << endl;
        for (int j = 0; j < this->power_problem_size; j++)
        {


            if (count_zeros_in_binary_representation(j) == i)
            {
                //cout << "checking " << j << endl;
                // cout << "> " << j << "  " << i << endl;
                for (int k = 0; k < this->num_of_cities; k++)
                {

                    if (std::find(already_visited.begin(), already_visited.end(), k) != already_visited.end())
                        continue;

                    int cost = connections[k][j];
                    
                    if (already_visited.size() != 0)
                        cost += connections[k][j] + map[k][already_visited.back()];
                    
                    if (cost != -1)
                    {

                        if (cost < min_cost)
                        {
                            min_cost = cost;
                            vertice_with_min_cost = k;
                        }
                    }
                }
            }
        }
        //cout << "Found smallest value " << min_cost << " in " << vertice_with_min_cost << endl;
        already_visited.push_back(vertice_with_min_cost);
    }

    already_visited.push_back(0);
    this->best_path = already_visited;
}

void DPSolver::solve(arrType map)
{
    int start_vertice = 0;
    this->map = map;

    num_of_cities = map.size();
    this->power_problem_size = pow(2, num_of_cities);

    int result = 0;

    vector<int> result_path;
    result_path.push_back(start_vertice);

    connections.clear();
    //paths.clear();

    for (int i = 0; i < num_of_cities; i++)
    {
        vector<int> connections_row;
        //vector<int> paths_row;

        for (long j = 0; j < power_problem_size; j++)
        {
            connections_row.push_back(-1);
            //paths_row.push_back(-1);
        }

        connections.push_back(connections_row);
        //paths.push_back(paths_row);
    }

    for (int i = 0; i < num_of_cities; i++)
    {
        connections[i][0] = map[i][0];
    }

    result = recurring_tsp_step(
        start_vertice,
        power_problem_size - 2);

    this->min_cost = result;
    restore_path();

    /*
    cout << endl << endl;
    for (int i = 0; i < connections[0].size(); i++)
    {
            //std::bitset<6> x(i);
            //cout << setw(6) << x << ", ";
            cout << setw(6) << i << ", ";
     
    }
    
        cout << endl;
    
    for (int i = 0; i < connections.size(); i++)
    {
        for (int j = 0; j < connections[i].size(); j++)
        {
            //if(connections[i][j] != -1)
                cout << setw(6) << connections[i][j] << ", ";
        }
        cout << endl;
    }
    
    cout << endl << endl;
    */
}

int DPSolver::recurring_tsp_step(int start, long set)
{
    long mask;
    long masked;

    int result = -1;

    int temp;

    if (this->connections[start][set] != -1)
    {
        return connections[start][set];
    }
    else
    {
        for (int i = 0; i < this->num_of_cities; i++)
        {
            mask = this->power_problem_size - 1 - pow(2, i);

            masked = set & mask;

            if (masked != set)
            {
                temp = this->map[start][i] +
                       recurring_tsp_step(
                           i,
                           masked);

                if (result == -1 || result > temp)
                {
                    result = temp;
                    //paths[start][set] = i;
                }
            }
        }

        connections[start][set] = result;

        return result;
    }
}