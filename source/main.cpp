#include <iostream>
#include "../include/Timer.h"
// #include"../include/ResourceMgr.h"
#include "../include/OutputMgr.h"
#include "../include/BFSolver.h"
#include "../include/BBSolver.h"
#include "../include/DPSolver.h"

// #include <iomanip> // std::setprecision

using namespace std;

void print(char *c)
{
    cout << c << endl;
}

int main(int argc, char **argv)
{

    if (argc == 3)
    {

        auto method = argv[1];
        int size = atoi(argv[2]);

        print(method);

        if (strcmp(method, "bf") == 0)
        {
            print("BruteForce method");

            auto graph = ResourceMgr::readGraph();

            // DPSolver::solve(graph);
            // OutputMgr::showGraph(graph);

            BFSolver solver;

            Timer t;
            t.start();
            solver.solve(graph);
            t.stop();

            solver.display_result();
            t.printTimeInSeconds();
        }
        else if (strcmp(method, "bb") == 0)
        {
            print("Branch and Bound method");

            auto graph = ResourceMgr::readGraph();

            // DPSolver::solve(graph);
            // OutputMgr::showGraph(graph);

            BBSolver solver;

            Timer t;
            t.start();
            solver.solve(graph);
            t.stop();


            solver.display_result();
            t.printTimeInSeconds();
        }
        else if (strcmp(method, "dp") == 0)
        {
            print("Dynamic programming method");

            auto graph = ResourceMgr::readGraph();

            // DPSolver::solve(graph);
            // OutputMgr::showGraph(graph);

            DPSolver solver;

            Timer t;
            t.start();
            solver.solve(graph);
            t.stop();


            solver.display_result();
            t.printTimeInSeconds();

            // cout.setf(ios::fixed); //, ios::floatfield);
            //cout.setf(ios::showpoint);
            // t.printTimeInSeconds();
        }
        else if (strcmp(method, "all") == 0)
        {
            
            print("All methods");

            auto graph = ResourceMgr::generateRandomGraph(size);

            // DPSolver::solve(graph);
            OutputMgr::showGraph(graph);

            
                    
            Timer t;
            
            cout << "Solving with BB" << endl;
            BBSolver bbSolver;
            
            t.start();
            bbSolver.solve(graph);
            t.stop();

            bbSolver.display_result();
            t.printTimeInSeconds();

            cout << "Solving with BF" << endl;
            BFSolver bfSolver;

            t.start();
            bfSolver.solve(graph);
            t.stop();

            bfSolver.display_result();
            t.printTimeInSeconds();

            cout << "Solving with DP" << endl;
            DPSolver dpSolver;

            t.start();
            dpSolver.solve(graph);
            t.stop();

            dpSolver.display_result();
            t.printTimeInSeconds();
            
        }
        else
        {
            print("no bf");
        }

        exit(0);
    }

    // exit(0);

    // Pun intended
    //auto graph = ResourceMgr::readGraph();
    // auto graph = ResourceMgr::generateRandomGraph(12);
    auto graph = ResourceMgr::readGraph();

    // DPSolver::solve(graph);
    // OutputMgr::showGraph(graph);

    // Timer t;
    // t.start();
    // BFSolver::solve(graph);
    // t.stop();

    // std::cout << t.getTimeInMicroSec() << std::endl;

    Timer t;

    cout << "Solving with BB" << endl;
    BBSolver bbSolver;

    t.start();
    bbSolver.solve(graph);
    t.stop();

    bbSolver.display_result();
    t.printTimeInSeconds();

    cout << "Solving with BF" << endl;
    BFSolver bfSolver;

    t.start();
    bfSolver.solve(graph);
    t.stop();

    bfSolver.display_result();
    t.printTimeInSeconds();

    cout << "Solving with DP" << endl;
    DPSolver dpSolver;

    t.start();
    dpSolver.solve(graph);
    t.stop();

    dpSolver.display_result();
    t.printTimeInSeconds();
    // std::cout << std::setprecision(1) << t2.getTimeInMicroSec() << std::endl;
}
