#include "../include/BFSolver.h"


int BFSolver::get_current_permutation_cost()
{

    // print(permutations);

    int cost = 0;

    int for_limit = permutations.size() - 1;
    for (int i = 0; i < for_limit; i++)
    {
        // cout << "from " << i << " to " << i+1 << endl; 
        cost += this->map[this->permutations[i]][this->permutations[i + 1]];
    }

    // cout << cost << endl;
    return cost;
}

void BFSolver::solve(arrType map)
{
    this->map = map;

    // print(permutations);

    permutations.clear();


    for (int i = 0; i < this->map.size(); i++)
    {
        permutations.push_back(i);
    }
    permutations.push_back(0); // the last element in the path

    auto permutations_start = permutations.begin() + 1;
    auto permutations_end = permutations.end() - 1;

    this->min_cost = INT_MAX;

    do
    {
        int cost = get_current_permutation_cost();
        // print(permutations);

        if (cost < this->min_cost)
        {
            this->min_cost = cost;

            best_path = permutations;
        }

    } while (std::next_permutation(permutations_start, permutations_end));
}