#include"../include/OutputMgr.h"

void OutputMgr::showGraph(vector<vector<int>> vec)
{

    for(auto& subVec : vec)
    {
        for(auto& val : subVec)
            cout<<val<<" ";
        cout<<endl;
    }
}